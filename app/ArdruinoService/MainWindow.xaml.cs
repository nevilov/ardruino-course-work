﻿using System.Windows;
using System.Windows.Controls;

namespace ArdruinoService;

/// <summary>
/// Interaction logic for MainWindow.xaml
/// </summary>
public partial class MainWindow : Window
{
    private readonly SerialPortService _serialPortService;
    private readonly State _state;

    public MainWindow()
    {
        InitializeComponent();
        _serialPortService = new SerialPortService();
        _state = State.Instance;
    }

    private void Window_Loaded(object sender, RoutedEventArgs e)
    {
        
    }


    private void SerialPortList_DropDownOpened(object sender, EventArgs e)
    {
        SerialPortList.Items.Clear();

        _serialPortService
            .GetAvailabelPorts()
            .ToList()
            .ForEach(x => SerialPortList.Items.Add(x));

    }

    private void SerialPortList_SelectionChanged(object sender, SelectionChangedEventArgs e)
    {
        var cmb = (ComboBox)sender;
        var port = (string) cmb.SelectedValue;

        if(string.IsNullOrEmpty(port) )
        {
            return;
        }

        _state.SerialPortName = port;
        _serialPortService.Connect(port, HandleSerialPortMessages);
        
    }

    public void HandleSerialPortMessages(string msg)
    {
        this.Dispatcher.Invoke(() => HandleMessagePortInternal(msg));
    }

    public void HandleMessagePortInternal(string msg)
    {
        SerialPortOutput.Text += $"{msg}";
        SerialOutputScroller.ScrollToBottom();

        SaveData(msg);
        PopulateData();
        UpdateStatistics();
    }

    private void UpdateStatistics()
    {
        MiddleGasValue.Text = _state.SensorsData.TryGetValue("GasSensor", out var gasValues) 
            ? (gasValues.Sum() / gasValues.Count).ToString()
            : "no_data";


        MiddleLightValue.Text = _state.SensorsData.TryGetValue("LightSensor", out var lightValues)
            ? (lightValues.Sum() / lightValues.Count).ToString()
            : "no_data";

        MiddleTemperatureValue.Text = _state.SensorsData.TryGetValue("TemperatureSensor", out var temperatureValues)
            ? (temperatureValues.Sum() / temperatureValues.Count).ToString()
            : "no_data";
    }

    public void SaveData(string msg)
    {
        var splitted = msg.Split(":");

        var definedItems = new List<string>()
        {
            "GasSensor",
            "TemperatureSensor",
            "LightSensor",
            "PirSensor",
            "LoudSensor",
            "UltrasonicSensor"
        };

        if (!definedItems.Contains(splitted[0])){
            return;
        }

        var value = int.Parse(splitted[1]);
        if (_state.SensorsData.ContainsKey(splitted[0]))
        {
            _state.SensorsData[splitted[0]].Add(value);
            return;
        }

        _state.SensorsData.Add(splitted[0], new List<int> { value });
    }

    public void PopulateData()
    {
        GasSensor.Text = _state.SensorsData.TryGetValue("GasSensor", out var gasSensorValues)
            ? gasSensorValues.Last().ToString()
            : "no_data";

        TemperatureSensor.Text = _state.SensorsData.TryGetValue("TemperatureSensor", out var temperatureSensor)
            ? temperatureSensor.Last().ToString()
            : "no_data";

        UltrasonicSensor.Text = _state.SensorsData.TryGetValue("UltrasonicSensor", out var ultrasonicSensor)
            ? ultrasonicSensor.Last().ToString()
            : "no_data";

        PirSensor.Text = _state.SensorsData.TryGetValue("PirSensor", out var pirSensor)
            ? pirSensor.Last().ToString()
            : "no_data";

        LightSensor.Text = _state.SensorsData.TryGetValue("LightSensor", out var lightSensor)
            ? lightSensor.Last().ToString()
            : "no_data";

        LoudSensor.Text = _state.SensorsData.TryGetValue("LoudSensor", out var loudSensor)
            ? loudSensor.Last().ToString()
            : "no_data";
    }
}