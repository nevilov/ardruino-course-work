﻿namespace ArdruinoService
{
    public class State
    {
        private State() { }

        public static readonly State Instance = new State();

        public string SerialPortName { get; set; }

        public IDictionary<string, List<int>> SensorsData { get; set; } = new Dictionary<string, List<int>>();
    }
}
