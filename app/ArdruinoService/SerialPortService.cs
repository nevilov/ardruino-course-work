﻿using System.IO.Ports;

namespace ArdruinoService
{
    public class SerialPortService : IDisposable
    {
        private SerialPort? _serialPort = null;
        private bool _isOpen = false;

        public bool Connect(string portName, Action<string> handleMessage, int baudRate = 9600)
        {
            if (_isOpen)
            {
                return true;
            }

            try
            {
                _serialPort = new SerialPort(portName, baudRate, Parity.None, 8, StopBits.One)
                {
                    WriteTimeout = 500,
                    DtrEnable = true
                };

                _serialPort.DataReceived += new SerialDataReceivedEventHandler((sender, ev) =>
                                    OnDataReceiveHandler(sender, ev, handleMessage));
                _serialPort.Open();
                _isOpen = true;

                return true;
            }
            catch (Exception ex)
            {
                throw new Exception("An error occured", ex);
            }
        }

        public void Close()
        {
            _serialPort!.Close();
        }

        public string[] GetAvailabelPorts()
        {
            return SerialPort.GetPortNames();
        }

        private void OnDataReceiveHandler(object sender, SerialDataReceivedEventArgs e, Action<string> handleMessage)
        {
            string data = _serialPort!.ReadLine();
            handleMessage(data);
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (_serialPort != null)
                {
                    _serialPort.Dispose();
                    _serialPort = null;
                }
            }
        }
    }
}
